#include<stdio.h>
#include<iostream>
#include <typeinfo>
#include <cmath>
#include <chrono>

using namespace std::chrono;
using Clock = std::chrono::steady_clock;

using namespace std;

class vector2 {
	public:
		float posX;
		float posY;
		float length;
		vector2(float x, float y) {
			posX = x;
			posY = y;
			length = sqrt((posX * posX) + (posY * posY));
		}
		void updateLength() {
			length = sqrt((posX * posX) + (posY * posY));
		}
};

float dotProduct(vector2 vec1, vector2 vec2) {
	return(float)(vec1.posX * vec2.posX + vec1.posY * vec2.posY);
}


vector2 getUnitVec(vector2 vector) {
	vector2 unitVec(vector.posX / vector.length, vector.posY / vector.length);
		return unitVec;
}


vector2 subctractVec(vector2 firstVec, vector2 secondVec) {
	vector2 result(firstVec.posX - secondVec.posX, firstVec.posY - secondVec.posY);
	return result;
}

vector2 addVec(vector2 firstVec, vector2 secondVec) {
	vector2 result(firstVec.posX + secondVec.posX, firstVec.posY + secondVec.posY);
	return result;
}

//calculations the projection of firstVec onto secondVec
vector2 projection(vector2 firstVec, vector2 secondVec) {
	//calculates the length of the projection of pos1ToPoint onto lineVec
	float projectionLength = (float)dotProduct(firstVec, secondVec) / (float)dotProduct(secondVec, secondVec);

	//find a vector that is on the same line as the line vector, but has a length where its end, its beginning, and the circle's centre form a right triangle
	vector2 projectionVec(secondVec.posX * projectionLength, secondVec.posY * projectionLength);
	return projectionVec;
}

vector2 getShortestDist(vector2 centre, vector2 pos1, vector2 pos2) {

	//creates a vector between the two Points and a vector between Point one and the circle's centre
	vector2 posToPoint = subctractVec(centre, pos1);
	vector2 lineVec = subctractVec(pos2, pos1);

	vector2 projectionVec = projection(posToPoint,lineVec);
	
	//returns a vector from the closest point to the centre of the circle
	vector2 projToPoint = subctractVec(projectionVec,posToPoint);

	return projToPoint;
}

int intersections(float distToCentre, int radius) {
	if (distToCentre < radius) {
		return 2;
	}
	else if (distToCentre == radius) {
		return 1;
	}
	return 0;
}

//finds a vector that connects the closest point on the line to the intersect point. Called this because this distance was labelled X on all my diagrams.
vector2 findDistX(float radius, vector2 L, vector2 lineVec) {
	float lLength = sqrt((radius * radius) - (L.length * L.length));
	vector2 unitLineVec = getUnitVec(lineVec);
	vector2 xVec(unitLineVec.posX * lLength, unitLineVec.posY * lLength);
	return xVec;

}

int main() {

	int radius = 10;

	float posOneX;
	float posOneY;
	float posTwoX;
	float posTwoY;
	float posCentreX;
	float posCentreY;

	while (true) {
		cout << "enter x value for Point one: ";

		cin >> posOneX;

		if (cin.fail()) {
			cin.clear(); 
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	while (true) {
		cout << "enter y value for Point one: ";

		cin >> posOneY;

		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	vector2 testPointOne(posOneX, posOneY);

	cout << "\nVector one: (" << testPointOne.posX << ", " << testPointOne.posX << ")\n\n";

	while (true) {
		cout << "enter x value for Point two: ";

		cin >> posTwoX;

		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	while (true) {
		cout << "enter y value for Point two: ";

		cin >> posTwoY;

		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	vector2 testPointTwo(posTwoX, posTwoY);

	cout << "\nVector two: (" << testPointTwo.posX << ", " << testPointTwo.posX << ")\n\n";


	vector2 lineVec = subctractVec(testPointTwo, testPointOne);
	
	while (true) {
		cout << "enter x value for centre of circle: ";

		cin >> posCentreX;

		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	while (true) {
		cout << "enter y value for centre of circle: ";

		cin >> posCentreY;

		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	while (true) {
		cout << "enter radius of circle: ";

		cin >> radius;

		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Re-enter a valid value! " << endl;
		}
		else break;
	}

	auto tic = Clock::now();

	vector2 testCentre(posCentreX, posCentreY);

	vector2 shortestDist = getShortestDist(testCentre, testPointOne, testPointTwo);

	int intersectNum = intersections(shortestDist.length, radius);

	cout << "\n\nNumber of intersections: " << intersectNum << "\n";

	vector2 crossPointOne(0, 0);
	vector2 crossPointTwo(0, 0);

	if (intersectNum == 1) {
		//if there is one intersection, then it must be at the point where the line is closest to the circle - calculated below using the projectino function.
		cout << "\n\Intersects circle at: (" << projection(subctractVec(testCentre, testPointOne), lineVec).posX << ", " << projection(subctractVec(testCentre, testPointOne), lineVec).posY << ")\n";
	}
	
	else if (intersectNum == 2) {

		//if you were to draw a line between the two intersection points and draw a dot in the middle, this code finds a vector between the dot and one of the intersections.
		//it then both adds and subtracts this vector from the dot in order to find the two intersection points
		//this bit of code got really messy - if I hadn't run out of time, I would have liked to put most of these calculations into a function to make it neater
		crossPointOne = addVec(findDistX(radius, shortestDist, lineVec), shortestDist);
		cout << "\n\Intersects circle at: (" << projection(subctractVec(testCentre,testPointOne), lineVec).posX - crossPointOne.posX << ", " << projection(subctractVec(testCentre, testPointOne), lineVec).posY + crossPointOne.posY << ")\n";
		cout << "\n\Intersects circle at: (" << projection(subctractVec(testCentre, testPointOne), lineVec).posX + crossPointOne.posX << ", " << projection(subctractVec(testCentre, testPointOne), lineVec).posY + crossPointOne.posY << ")\n";
	}


	
	auto toc = Clock::now();

	cout << "\nRuntime: " << duration_cast<milliseconds>(toc - tic).count() << "\n\n" ;
	
	/*
	notes on the other challenges:
	I did some research on test suites, but creating one seemed somewhat daunting, with most tutorials advising the creation of a whole new project
	or the downloading of new software. I imagine there is a simpler way, but unfortunately I could not complete this task.

	Adapting this function into 3d seemed relatively simple - it would involve the conversion of all vector 2s into vector 3s as well as additional
	calculations for the extra variable. I suppose this is easier said than done, but seems manageable nonetheless.


	I had devised a plan to implement the challenge of finding a path around the circle,
	but ran out of time to implement it :(. 

	it should be noted that this plan involves getting halfway around the circle, after which the fastest route is not to continue until the other
	intersection point is reached, but to instead take a direct route towards the endpoint



	THE PLAN: 

	The plan first asks the user how many segments they would like the path to be comprised of. The minimum number would be two - as soon as it hits
	the 
	-first, a constant takes the number of subdivisions to create the path around the circle.
		-the minimum number of segments would be 2 - the path takes a 90 degree turn upon hitting the edge of the circle, then goes up until it has 
		a straight path to the end segment.

	-using calculations that were surprisingly simple once written on paper, with nothing but the number of subdivisions, one can calculate the angle
	between each set of two points as well as the distance between them. From there, it would be a simple matter of converting angles and distances
	into vectors.

	
	*/

	return 0;
}